# fakturowanie

Application for invoicing

- [ ] User must log in to see his invoices
- [ ] User cad add/modify/delete product
- [ ] User can add/modify/delete invoice
- [ ] User can export invoice to PDF


Optional features:
    User can change currency of invoice



## Plan pracy

### Backend Framework

    DJAGNO as framework
    
### Frontend:
    Foundation CSS + Crispy Forms
    https://get.foundation/
    Dashboard - https://get.foundation/building-blocks/blocks/grid-staggered.html
### Database:
    Google Firestore as database:
    https://googleapis.dev/python/firestore/latest/index.html
    https://firebase.google.com/docs/firestore

## Login Page
- [ ] Default root app is /login
- [ ] Create DB project for Sign In options
- [ ] After login redirect to /dashboard

## Customers
- [ ] Default root app is /login
- [ ] Create DB project for Sign In options
- [ ] After login redirect to /dashboard

## Application Project
- [ ] DB Project
- [ ] Add first particular rows
- [ ] Generate first view with particular data from DB
- [ ] Add first data from front
