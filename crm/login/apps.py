from django.apps import AppConfig


class KlienciConfig(AppConfig):
    name = 'crm.login'
