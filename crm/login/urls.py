from django.urls import path

from . import views

urlpatterns = [
    path('', views.login, name='login'),
    path("auth", views.authenticationfunc, name='authenticationfunc'),
]