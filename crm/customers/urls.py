from django.urls import path
from crm.customers.views import CustomersView
from . import views

urlpatterns = [
    path('add_customer', views.add_customer, name='add_customers'),

    path('', CustomersView.as_view(), name='customers'),
    path('customers/delete/<customer_id>',views.delete_customer,name='delete'),
]