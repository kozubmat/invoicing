from django.db import models

# Create your models here.


from django.db import models

class Customers(models.Model):
    id = models.AutoField(primary_key=True)
    shortname = models.CharField(max_length=64)
    longname = models.CharField(max_length=512)
    address_street = models.CharField(max_length=256)
    address_number = models.CharField(max_length=4)
    address_local_number = models.IntegerField()
    address_postal_code = models.IntegerField()
    address_city = models.CharField(max_length=64)
    tax_number = models.PositiveIntegerField()
    regon = models.PositiveIntegerField()

   #def __str__(self):
   #    return tuple(self.id, self.shortname, self.tax_number)

