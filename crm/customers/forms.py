from django import forms

class AddCustomerForm(forms.Form):
    shortname = forms.CharField(label='Krotka Nazwa Firmy', max_length=64)
    longname = forms.CharField(label='Dluga Nazwa Firmy', max_length=512)
    address_street = forms.CharField(label='Ulica', max_length=256)
    address_number = forms.CharField(label='Numer domu', max_length=4)
    address_local_number = forms.IntegerField(label='Numer mieszkania')
    address_postal_code = forms.IntegerField(label='Kod Pocztowy')
    address_city = forms.CharField(label='Miasto', max_length=64)
    tax_number = forms.IntegerField(label='NIP')
    regon = forms.IntegerField(label='REGON')

