from rest_framework import serializers

from .models import Customers



class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customers
        fields = ('id', 'shortname', 'longname', 'address_street','address_number', 'address_local_number','address_postal_code', 'address_city', 'tax_number','regon')
