from django.http import HttpResponseRedirect
from crm.customers.models import Customers
from django.views.generic import ListView
from .forms import AddCustomerForm
from django.shortcuts import render, redirect


# Create your views here.

#from django.http import HttpResponse
#
#
#def index(request):
#    return HttpResponse("Hello, world. You're at the polls index.")

#def customers(request):
#    return render(request, 'customers.html')
from django.urls import reverse


class CustomersView(ListView):
    paginate_by = 5
    model = Customers
    template_name = 'customers.html'
    context_object_name = 'customers'



def add_customer(request):
    if request.method == 'POST':
        form = AddCustomerForm(request.POST)
        if form.is_valid():
            _shortname = request.POST.get('shortname')
            _longname = request.POST.get('longname')
            _address_street = request.POST.get('address_street')
            _address_number = request.POST.get('address_number')
            _address_local_number = request.POST.get('address_local_number')
            _address_postal_code = request.POST.get('address_postal_code')
            _address_city = request.POST.get('address_city')
            _tax_number = request.POST.get('tax_number')
            _regon = request.POST.get('regon')
            _customer = Customers(shortname=_shortname,
                                  longname=_longname,
                                  address_street=_address_street,
                                  address_number=_address_number,
                                  address_local_number=_address_local_number,
                                  address_postal_code=_address_postal_code,
                                  address_city=_address_city,
                                  tax_number=_tax_number,
                                  regon=_regon)
            _customer.save()
            return redirect('customers')
    else:
        form = AddCustomerForm()
    return render(request, 'add_customers.html', {'form': form})

def delete_customer(request, customer_id = None):
    customer_to_delete = Customers.objects.get(id=customer_id)
    customer_to_delete.delete()
    return redirect('customers')


    # name of the view function that returns your posts page)
#def AddCustomerSubmission(request):
#    shortname = request.POST.get('shortname')
#    longname =
#    address_street =
#
#
#
#    UserModel = get_user_model()
#    user = get_object_or_404(UserModel, email=_email)
#    if user.check_password(_password):
#        return render(request, 'dashboard.html')
#
#    return render(request, 'add_customers.html'