from django.test import TestCase
from django.test import Client
from .forms import AddCustomerForm
from http import HTTPStatus

from django.urls import reverse

####
#### ZAWSZE TESTUJEMY FUNKCJONALNOSCI (!)
####

class CustomersTestCase(TestCase):
    def test_create_ser_with_form(self):
        c = Client()
        form_data  = {
            "shortname": "Firma1",
            "longname": "Dluga nazwa Firma 1",
            "address_street": "Ulicyjna",
            "address_number": "1",
            "address_local_number": 1,
            "address_postal_code": "11111",
            "address_city": "Jedynkowa",
            "tax_number": 1234567890,
            "regon": 123456789
        }
        form = AddCustomerForm(data=form_data)
        self.assertTrue(form.is_valid())
        response = c.post(reverse("add_customers"), form_data)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

